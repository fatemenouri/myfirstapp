import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute} from '@angular/router';
@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.sass']
})
export class WelcomeComponent implements OnInit {

  constructor(
    ) { }
  customerName: string =''; 
  inputFieldValue :string='';
  ngOnInit(): void {

  }
  saveCustomerName(value:string){
    this.customerName=this.inputFieldValue
  }

}
