import { Component } from '@angular/core';
import {NgForm} from '@angular/forms';
import { Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  constructor(private router: Router) { }
  onSubmit(form: NgForm) {
    console.log('Your form data : ', form.value);
    this.router.navigate(['/welcome'], { queryParams: form.value})
  }
}
